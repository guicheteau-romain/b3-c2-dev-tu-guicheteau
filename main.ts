import { simplifySigne, Signe } from './utils/utils.signe.ts'
import { isOperator, Operator } from './utils/utils.operator.ts'

function chekcaracteres(list: string[]) {
    const allowedChars: string[] = ['+', '-', '*', '/', '%', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', ' ', '(', ')', '.']

    let preChar = list[0]
    for (let i = 0; i < list.length; i++) {
        const element = list[i]

        if (!allowedChars.includes(element)) {
            throw new Error('il semlbe y avoir une erreur de syntaxe avec des caractères non autorisés')
        }
        // check if double operator
        else if (i != 0 && isOperator(preChar) && isOperator(element)) {
            throw new Error('il semlbe y avoir une erreur de syntaxe avec les Operator : 2 Operator consécutifs')
        } else if (i != 0 && isOperator(preChar) && preChar == Operator.DIVISION && element == '0' && list[i + 1] != '.') {
            throw new Error('division par 0 impossible')
        } else preChar = element
    }
}

export function calculate(iniOperation: string) {
    let operation = iniOperation.replaceAll(' ', '')
    try {
        chekcaracteres(operation.split(''))
        operation = simplifySigne(operation.split(''))
    } catch (error) {
        return error
    }
    // this line is for debug
    console.log('\n', iniOperation, '\n=', operation, '=', eval(operation))
    const result = eval(operation)
    if (result == Infinity) {
        return Error('division par 0 impossible')
    }

    return result
}

const input = prompt()
if (typeof input == 'string') {
    console.log(calculate(input))
} else console.log('type de donnée non valide')
