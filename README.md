# b3-c2-dev-tu-guicheteau

ceci est un projet ou j'ai due realiser les test unitaires d'une calculatrice.

pour ce faire j'ai utilise deno.

lang : TypeScript

## install deno :

Linux / macOs

```bash
curl -fsSL https://deno.land/x/install/install.sh | sh
```

Windows

```bash
irm https://deno.land/install.ps1 | iex
```

Autrement allez directement sur la
[documentation](https://deno.land/manual@v1.31.2/getting_started/installation)
de deno

## Lancement :

lancement des **Unit test**

```sh
deno test
```

lancement des **script**

```sh
deno run main.ts
```

## Realisation

Pour realiser ce projet, j'ai commence par écrire toute sortes d'operations.

Exemple pour les soustraction :

```ts
Deno.test("Soustact Test", () => {
  assertEquals(calculate("1-1"), 0, "1-1 should be 0");
  assertEquals(calculate("-1-1"), -2, "-1-1 should be -2");
  assertEquals(calculate("1-2"), -1, "1-2 should be -1");
  assertEquals(calculate("2-1"), 1, "2-1 should be 1");
  assertEquals(calculate("1--2"), 3, "1--2 should be 3");
  assertEquals(calculate("1+-2"), -1, "1+-2 should be -1");
  assertEquals(calculate("1-+2"), -1, "1-+2 should be -1");
  assertEquals(calculate("(2-1)-1"), 0, "(2-1)-1 should be 0");
  assertEquals(calculate("(2-1)--1"), 2, "(2-1)--1 should be 2");
  assertEquals(
    calculate("(9-8)--(10-+5-(5-5))"),
    6,
    "(9-8)--(10-+5-(5-5)) should be 6",
  );
  assertEquals(calculate("1-1-1-1-1"), -3, "1-1-1-1-1 should be -3");
});
```

une fois avoir ecrit ces test, je me suis mis à tester mon code en elliminant
les eventuelles problemes de syntax du style `1%/1`, `=`, `%%` donc je bloque le
fait que l'on puisse faire deux fois un operateur.

j'ai separé deux types "d'operateur" les `Operateur` et les `Signes` la
difference etant que les signes peuvent se succeder exemple : `+-` et dans ce
cas une règle de signe s'applique tendis que les operateurs eux ne peuvent pas
être succesif.

j'ai donc crée la methode `simplifySigne` qui vas formater la string de mon operation final pour que la methode eval puisse la prendre en compte.

le test sur les pourcentages ne fonctionne pas car les pourcentages n'ont pas été implementé.
