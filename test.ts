import { assert, assertEquals, assertIsError } from 'https://deno.land/std@0.179.0/testing/asserts.ts'
import { calculate } from './main.ts'

Deno.test('Syntax Test', () => {
    // good test
    assertEquals(calculate('1 - 1'), 0, '1-1 should be 0')

    // bad test
    assertIsError(calculate('1**2'))
    assertIsError(calculate('1//2'))
    assertIsError(calculate('1%%2'))
    assertIsError(calculate('1 x 2'))
    assertIsError(calculate('1+a'))
})
Deno.test('Soustact Test', () => {
    assertEquals(calculate('1-1'), 0, '1-1 should be 0')
    assertEquals(calculate('-1-1'), -2, '-1-1 should be -2')
    assertEquals(calculate('1-2'), -1, '1-2 should be -1')
    assertEquals(calculate('2-1'), 1, '2-1 should be 1')
    assertEquals(calculate('1--2'), 3, '1--2 should be 3')
    assertEquals(calculate('1+-2'), -1, '1+-2 should be -1')
    assertEquals(calculate('1-+2'), -1, '1-+2 should be -1')
    assertEquals(calculate('(2-1)-1'), 0, '(2-1)-1 should be 0')
    assertEquals(calculate('(2-1)--1'), 2, '(2-1)--1 should be 2')
    assertEquals(calculate('(9-8)--(10-+5-(5-5))'), 6, '(9-8)--(10-+5-(5-5)) should be 6')
    assertEquals(calculate('1-1-1-1-1'), -3, '1-1-1-1-1 should be -3')
})

Deno.test('Addition Test', () => {
    assertEquals(calculate('1+1'), 2, '1+1 should be 2')
    assertEquals(calculate('+1+2'), 3, '1+2 should be 3')
    assertEquals(calculate('2++1'), 3, '2++1 should be 3')
    assertEquals(calculate('2+1+1+1+1+1+1+1+1+1'), 11, '2+1+1+1+1+1+1+1+1+1 should be 11')
})

Deno.test('Multiplication Test', () => {
    assertEquals(calculate('1*1'), 1, '1*1 should be 1')
    assertEquals(calculate('2*0.5'), 1, '2*0.5 should be 1')
    assertEquals(calculate('1*-1'), -1, '1*-1 should be -1')
    assertEquals(calculate('-1*1'), -1, '-1*1 should be -1')
    assertEquals(calculate('-1*-1'), 1, '-1*-1 should be 1')
    assertEquals(calculate('1*0'), 0, '1*0 should be 0')
    assertEquals(calculate('(1*-(2*5))'), -10, '(1*-(2*5)) should be -10')
    assertEquals(calculate('2.25*(-1.75*+(2.56*(5.50--5.85)))*0.5+8.56'), -48.644, '2.25*(-1.75*+(2.56*(5.50--5.85)))*0.5+8.56 should be -48.644')
})

Deno.test('Division Test', () => {
    assertEquals(calculate('1/1'), 1, '1*1 should be 1')
    assertEquals(calculate('2/1'), 2, '2/1 should be 2')
    assertEquals(calculate('0/1'), 0, '0/1 should be 0')
    assertEquals(calculate('0/1/3'), 0, '0/1/3 should be 0')
    assertEquals(calculate('1/2'), 0.5, '1/2 should be 0.5')
    assertEquals(calculate('1/-2'), -0.5, '1/-2 should be -0.5')
    assertEquals(calculate('-1/-2'), 0.5, '-1/-2 should be 0.5')
    assertEquals(calculate('1/4+4'), 4.25, '1/4+4 should be 4.25')
    assertEquals(calculate('1/(2+2)'), 0.25, '1/(2+2) should be 0.25')

    assertIsError(calculate('1/0'))
    assertIsError(calculate('1/(3-3)'))
    assertIsError(calculate('1/0/7'))
})

Deno.test('percent Test', () => {
    //  dont work
    // assertEquals(calculate('100-4%'), 96, '100-4% should be 96')
})

Deno.test('divers Test', () => {
    assertEquals(calculate('45*3+(5+1)/1'), 141, '45*3+(5+1)/1 should be 141')
    assertEquals(calculate('45*3+(5+1)/1'), 141, '45*3+(5+1)/1 should be 141')
    assertIsError(calculate('45*3+(5+1)/0/7'))
})
