export enum Operator {
    // ADDITION = '+',
    // SOUSTRACTION = '-',
    MULTIPLICATION = '*',
    DIVISION = '/',
    POURCENT = '%',
}

export function isOperator(char: string): char is Operator {
    return Object.values(Operator).includes(char as Operator)
}
