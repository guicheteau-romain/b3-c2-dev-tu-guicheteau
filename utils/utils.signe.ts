export enum Signe {
    POSITIVE = '+',
    NEGATIVE = '-',
}

export function simplifySigne(list: string[]): string {
    let newlist: string[] = []
    let preChar = list[0]
    for (let i = 0; i < list.length; i++) {
        const element = list[i]
        if (i != 0 && isSigne(preChar) && isSigne(element)) {
            const newSigne = signeMixor(preChar, element)
            newlist.pop()
            newlist.push(newSigne)
            preChar = newSigne
        } else {
            preChar = element
            newlist.push(element)
        }
    }
    return newlist.join('')
}

function signeMixor(pre: string, actual: string) {
    switch (pre + ',' + actual) {
        case Signe.POSITIVE + ',' + Signe.POSITIVE:
            return Signe.POSITIVE
        case Signe.POSITIVE + ',' + Signe.NEGATIVE:
            return Signe.NEGATIVE
        case Signe.NEGATIVE + ',' + Signe.POSITIVE:
            return Signe.NEGATIVE
        case Signe.NEGATIVE + ',' + Signe.NEGATIVE:
            return Signe.POSITIVE
        default:
            return actual
    }
}

function isSigne(char: string): char is Signe {
    return Object.values(Signe).includes(char as Signe)
}
